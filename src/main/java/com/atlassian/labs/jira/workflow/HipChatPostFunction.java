package com.atlassian.labs.jira.workflow;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.fugue.Option;
import com.atlassian.hipchat.plugins.api.client.HipChatClient;
import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.util.ImportUtils;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.atlassian.query.Query;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.hipchat.plugins.api.client.Message.BackgroundColor;
import static com.atlassian.hipchat.plugins.api.client.Message.Format;

public class HipChatPostFunction extends AbstractJiraFunctionProvider
{

    public static final String NOTIFICATION_TEMPLATE_PATH = "/templates/postfunctions/hip-chat-notification.vm";
    public static final String CONFIG_ERROR_NOTIFICATION_TEMPLATE_PATH = "/templates/postfunctions/hip-chat-notification-error.vm";
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final SearchService searchService;
    private final ApplicationProperties applicationProperties;
    private final TemplateRenderer templateRenderer;
    private final HipChatConfigurationManager configurationManager;
    private final HipChatPostFunctionExecutorService executorService;
    private final HipChatClient hipChatClient;

    public HipChatPostFunction(ApplicationProperties applicationProperties, SearchService searchService,
            HipChatClient hipChatClient, TemplateRenderer templateRenderer, HipChatConfigurationManager configurationManager,
            HipChatPostFunctionExecutorService executorService)
    {
        this.applicationProperties = applicationProperties;
        this.hipChatClient = hipChatClient;
        this.searchService = searchService;
        this.templateRenderer = templateRenderer;
        this.configurationManager = configurationManager;
        this.executorService = executorService;
    }

    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        if (!configurationManager.getApiToken().isDefined()) {
            return;
        }

        Issue issue = getIssue(transientVars);

        WorkflowDescriptor descriptor = (WorkflowDescriptor) transientVars.get("descriptor");
        Integer actionId = (Integer) transientVars.get("actionId");
        ActionDescriptor action = descriptor.getAction(actionId);
        Issue originalIssue = (Issue) transientVars.get("originalissueobject");
        String firstStepName = "";
        if (originalIssue != null) {
            Status status = originalIssue.getStatusObject();
            firstStepName = status.getName();
        }

        String actionName = action.getName();
        StepDescriptor endStep = descriptor.getStep(action.getUnconditionalResult().getStep());

        Iterable<String> roomsToNotifyIds = Splitter.on(",").omitEmptyStrings().split(Strings.nullToEmpty((String) args.get(HipChatPostFunctionFactory.ROOMS_TO_NOTIFY_CSV_IDS_PARAM)));

        boolean notifyClients = Boolean.parseBoolean((String) args.get(HipChatPostFunctionFactory.NOTIFY_CLIENTS_PARAM));

        if (roomsToNotifyIds.iterator().hasNext()) {
            String jql = (String) args.get(HipChatPostFunctionFactory.JQL_FILTER_PARAM);

            try {
                final User caller = getCaller(transientVars, args);
                final NotificationDto notificationDto =
                        new NotificationDto(applicationProperties.getBaseUrl(), issue, caller, firstStepName, endStep, actionName);

                // if we have to check JQL we need to make sure the issue is in the DB _before_ we index it.
                if (!isIssueStoredInDatabase(issue) && !Strings.isNullOrEmpty(jql))
                {
                    sendErrorNotification(roomsToNotifyIds, notifyClients, notificationDto);
                }
                else if (Strings.isNullOrEmpty(jql))
                {
                    sendNotification(roomsToNotifyIds, notifyClients, notificationDto);
                }
                else if (matchesJql(jql, issue, caller))
                {
                    // the issues get indexed only after all the post functions have run. Doesn't work for us as
                    // we need to do JQL matching
                    ensureIndexed(issue);
                    sendNotification(roomsToNotifyIds, notifyClients, notificationDto);
                }
            } catch (SearchException e) {
                throw new WorkflowException(e);
            } catch (IOException e) {
                throw new WorkflowException(e);
            }
            catch (IndexException e)
            {
                throw new WorkflowException(e);
            }
        }
    }

    private void sendNotification(Iterable<String> roomsToNotifyIds, boolean notifyClients, NotificationDto notificationDto) throws IOException {
        StringWriter messageWriter = new StringWriter();
        templateRenderer.render(NOTIFICATION_TEMPLATE_PATH, ImmutableMap.<String, Object>of("dto", notificationDto), messageWriter);

        executorService.execute(new SendNotificationRunnable(hipChatClient, roomsToNotifyIds, messageWriter.toString(), notifyClients, logger, false));
    }

    private void sendErrorNotification(Iterable<String> roomsToNotifyIds, boolean notifyClients, NotificationDto notificationDto) throws IOException {
        StringWriter messageWriter = new StringWriter();
        templateRenderer.render(CONFIG_ERROR_NOTIFICATION_TEMPLATE_PATH, ImmutableMap.<String, Object>of("dto", notificationDto), messageWriter);

        executorService.execute(new SendNotificationRunnable(hipChatClient, roomsToNotifyIds, messageWriter.toString(), notifyClients, logger, true));
    }

    @VisibleForTesting
    protected boolean isIssueStoredInDatabase(Issue issue)
    {
        return issue.getId() != null;
    }

    // protected to allow unit testing
    protected boolean matchesJql(String jql, Issue issue, User caller) throws SearchException, IndexException
    {
        SearchService.ParseResult parseResult = searchService.parseQuery(caller, jql);
        if (parseResult.isValid())
        {
            Query query = JqlQueryBuilder.newBuilder(parseResult.getQuery())
                    .where()
                    .and()
                    .issue()
                    .eq(issue.getKey())
                    .buildQuery();

            return searchService.searchCount(caller, query) > 0;
        }

        return false;
    }

    private void ensureIndexed(Issue issue) throws IndexException
    {
        final boolean indexIssues = ImportUtils.isIndexIssues();
        ImportUtils.setIndexIssues(true);
        try
        {
            getIssueIndexManager().reIndex(issue);
        }
        finally
        {
            ImportUtils.setIndexIssues(indexIssues);
        }
    }

    @VisibleForTesting
    protected IssueIndexManager getIssueIndexManager()
    {
        return ComponentAccessor.getIssueIndexManager();
    }

    private static class SendNotificationRunnable implements Runnable {

        private final HipChatClient hipChatClient;
        private final Iterable<String> roomsToNotifyIds;
        private final String message;
        private final boolean notifyClients;
        private final Logger logger;
        private final boolean error;

        public SendNotificationRunnable(HipChatClient hipChatClient, Iterable<String> roomsToNotifyIds, String message, boolean notifyClients, Logger logger, boolean error) {
            this.hipChatClient = hipChatClient;
            this.roomsToNotifyIds = roomsToNotifyIds;
            this.message = message;
            this.notifyClients = notifyClients;
            this.logger = logger;
            this.error = error;
        }

        @Override
        public void run()
        {
            for (String roomId : roomsToNotifyIds)
            {
                final Option<Format> format = none();
                final Option<BackgroundColor> bgColor = error ? some(BackgroundColor.RED) : Option.<BackgroundColor>none();

                hipChatClient.rooms().message(roomId, "JIRA", message, format, bgColor, some(notifyClients));
            }
        }
    }

    public static class NotificationDto {

        private final String baseUrl;
        private final Issue issue;
        private final User actor;
        private final String firstStepName;
        private final StepDescriptor endStep;
        private final String actionName;

        public NotificationDto(String baseUrl, Issue issue, User actor, String firstStepName, StepDescriptor endStep, String actionName) {
            this.baseUrl = baseUrl;
            this.issue = issue;
            this.actor = actor;
            this.firstStepName = firstStepName;
            this.endStep = endStep;
            this.actionName = actionName;
        }

        public String getBaseUrl() {
            return baseUrl;
        }

        public Issue getIssue() {
            return issue;
        }

        public User getActor() {
            return actor;
        }

        public String getFirstStepName() {
            return firstStepName;
        }

        public StepDescriptor getEndStep() {
            return endStep;
        }

        public String getActionName() {
            return actionName;
        }
    }
}